/*
* Based On
* MIT Licensed
* http://www.23developer.com/opensource
* http://github.com/23/resumable.js
* Steffen Tiedemann Christensen, steffen@23company.com
*/

(function(window, undefined) {
	window.base64Uploader = function(opts) {
		if (!(this instanceof window.base64Uploader)) {
			return new window.base64Uploader(opts);
		}
			
		this.support = false;	
		this.flashSupport = false;
		this.support = (
			(typeof (window.File) !== 'undefined') &&
			(typeof (window.Blob) !== 'undefined') &&
			(typeof (window.FileList) !== 'undefined') &&
			( !! Blob.prototype.webkitSlice || !! Blob.prototype.mozSlice || Blob.prototype.slice || false)
		);

		try {
			this.flashSupport = Boolean(new ActiveXObject("ShockwaveFlash.ShockwaveFlash"))
		} catch(e) {
			this.flashSupport = typeof navigator.mimeTypes["application/x-shockwave-flash"] !== "undefined" 
		}

		// PROPERTIES
		var $ = this;
		$.files = [];

		$.defaults = {
			launchCamera:true,
			readAs: "data",
			chunkSize:1*1024*1024,
			forceChunkSize:false,
			simultaneousUploads:3,
			fileParameterName:'file',
			throttleProgressCallbacks:0.5,
			query:{},
			headers:{},
			preprocess:null,
			method:'multipart',
			prioritizeFirstAndLastChunk:false,
			target:'/',
			testChunks:true,
			generateUniqueIdentifier:null,
			maxChunkRetries:undefined,
			chunkRetryInterval:undefined,
			permanentErrors:[415, 500, 501],
			maxFiles:undefined,
			flashAlert:"To upload in this browser you will need to have Adobe Flash installed. To download, please follow this link: <a target='_blank' href='http://get.adobe.com/flashplayer/'>http://get.adobe.com/flashplayer/<a>",
			maxFilesErrorCallback:function (files, errorCount) {
				var maxFiles = $.getOpt('maxFiles');
				$.alert('Please upload ' + maxFiles + ' file' + (maxFiles === 1 ? '' : 's') + ' at a time.');
			},
			minFileSize:undefined,
			minFileSizeErrorCallback:function(file, errorCount) {
				$.alert(file.fileName +' is too small, please upload files larger than ' + $h.formatSize($.getOpt('minFileSize')) + '.');
			},
			maxFileSize:undefined,
			maxFileSizeErrorCallback:function(file, errorCount) {
				$.alert(file.fileName +' is too large, please upload files less than ' + $h.formatSize($.getOpt('maxFileSize')) + '.');
			},
			fileType: [],
			fileTypeErrorCallback: function(file, errorCount) {
				$.alert(file.fileName +' has type not allowed, please upload files of type ' + $.getOpt('fileType') + '.');
			}
		};

		$.opts = opts || {};
		$.alert = (AF && AF.alert) ? AF.alert.alert : window.alert;
		$.getOpt = function(o) {
			var $this = this;
			// Get multiple option if passed an array
			if(o instanceof Array) {
				var options = {};
				$h.each(o, function(option){
					options[option] = $this.getOpt(option);
				});
				return options;
			}
			// Otherwise, just return a simple option
			if ($this instanceof base64File) {
				if (typeof $this.opts[o] !== 'undefined') { return $this.opts[o]; }
				else { $this = $this.base64Obj; }
			}
			if ($this instanceof base64Uploader) {
				if (typeof $this.opts[o] !== 'undefined') { return $this.opts[o]; }
				else { return $this.defaults[o]; }
			}
		};

		// EVENTS
		// catchAll(event, ...)
		// fileSuccess(file), fileProgress(file), fileAdded(file, event), fileRetry(file), fileError(file, message),
		// complete(), progress(), error(message, file), pause()
		$.events = [];
		$.on = function(event, callback){
			$.events.push(event.toLowerCase(), callback);
		};

		$.off = function(event) {
			for (var i=0; i<=$.events.length; i+=2) {
				if($.events[i]==event) {
					$.events.splice(i, 2);
					break;
				}
			}
		};

		$.trigger = function(){
			$.fire.apply($, arguments);
		}	

		$.fire = function(){
			// `arguments` is an object, not array, in FF, so:
			var args = [];

			for (var i=0; i<arguments.length; i++) args.push(arguments[i]);
			// Find event listeners, and support pseudo-event `catchAll`
			var event = args[0].toLowerCase();
			for (var i=0; i<=$.events.length; i+=2) {
				if($.events[i]==event) $.events[i+1].apply($,args.slice(1));
				if($.events[i]=='catchall') $.events[i+1].apply(null,args);
			}
			if(event=='fileerror') $.fire('error', args[2], args[1]);
			if(event=='fileprogress') $.fire('progress');
		};

		// INTERNAL HELPER METHODS (handy, but ultimately not part of uploading)
		var $h = {
			stopEvent: function(e){
				e.stopPropagation();
				e.preventDefault();
			},
			each: function(o,callback){
				if(typeof(o.length)!=='undefined') {
					for (var i=0; i<o.length; i++) {
						// Array or FileList
						if(callback(o[i])===false) return;
					}
				} else {
					for (i in o) {
						// Object
						if(callback(i,o[i])===false) return;
					}
				}
			},
			generateUniqueIdentifier:function(file){
				var custom = $.getOpt('generateUniqueIdentifier');
				if(typeof custom === 'function') {
					return custom(file);
				}
				var relativePath = file.webkitRelativePath||file.fileName||file.name; // Some confusion in different versions of Firefox
				var size = file.size;
				return(size + '-' + relativePath.replace(/[^0-9a-zA-Z_-]/img, ''));
			},
			contains:function(array,test) {
				var result = false;

				$h.each(array, function(value) {
					if (value == test) {
						result = true;
						return false;
					}
					return true;
				});

				return result;
			},
			formatSize:function(size){
				if(size<1024) {
					return size + ' bytes';
				} else if(size<1024*1024) {
					return (size/1024.0).toFixed(0) + ' KB';
				} else if(size<1024*1024*1024) {
					return (size/1024.0/1024.0).toFixed(1) + ' MB';
				} else {
					return (size/1024.0/1024.0/1024.0).toFixed(1) + ' GB';
				}
			}
		};


		var mobile_upload = function(e){
			if(!navigator.camera || !navigator.camera.getPicture) return;

			var fail_upload = function(message){
				console.info("[Upload] camera.getPicture Error: " + message);
			};
			var do_upload = function(imageURI){

				var onSuccess = function(fileEntry) {

					var successFile = function(file) {
						$.addFile(file);
					};
					var failFile = function(message){
						console.info("[Upload] File Error: " + message);
					};
					/*console.info("Image Path:", fileEntry.fullPath, JSON.stringify(fileEntry));*/
					fileEntry.file(successFile, failFile);
				};

				var onFail = function(fileEntry){
					console.info("[Upload] resolveLocalFileSystemURI System Error: " + message);
				};

				window.resolveLocalFileSystemURI(imageURI, onSuccess, onFail); 		
			};

			
			var upload_options = {   
				quality: 50,
				destinationType: navigator.camera.DestinationType.FILE_URI,
                sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
				allowEdit: true
			};

            if($.getOpt('launchCamera')) {
                upload_options.sourceType = navigator.camera.PictureSourceType.CAMERA;
            }

			// Retrieve image file location from specified source
			navigator.camera.getPicture(do_upload, fail_upload, upload_options);
		};
		// INTERNAL OBJECT TYPES
		function base64File(base64Obj, file,support){
			var support = typeof support !== 'undefined' ?  support : true;
			var $ = this;
			$.opts = {};
			$._prevProgress = 0;
			$.base64Obj = base64Obj;
			$.getOpt = base64Obj.getOpt;
			if(support)$.fileReader = new FileReader();
			$.file = file;
			$.type = file.type;
			$.size = file.size;
			$.name = $.fileName = file.fileName || file.name; // Some confusion in different versions of Firefox
			$.relativePath = file.webkitRelativePath || $.fileName;
			$.uniqueIdentifier = $h.generateUniqueIdentifier(file);
			var _error = false;

			var arr = new String(file.type).split(/\//g);
			$._type = arr[0]; $._ext = arr[1];

			$._readAs = $.getOpt('readAs');
			if($._readAs === "text") 
				$._readAs = "readAsText"
			else if($._readAs === "data")
				$._readAs = "readAsDataURL"
			else {
				if($._type === "text")
					$._readAs = "readAsText"	
				else $._readAs = "readAsDataURL"				
			}			

			// Callback when something happens within the chunk
			var ev = function(event, message){
				// event can be 'progress', 'success', 'error' or 'retry'
				switch(event){
					case 'progress':
						$.base64Obj.fire('fileProgress', $);
						break;
					case 'error':
						$.abort();
						_error = true;
						$.base64Obj.fire('fileError', $, message);
						break;
					case 'success':
						if(_error) return;
						$.base64Obj.fire('fileProgress', $); // it's at least progress
						$.base64Obj.fire('fileSuccess', $, message);
						break;
					case 'retry':
						$.base64Obj.fire('fileRetry', $);
						break;
				}
			}

			$.cancel = function(){
				_error = true;
				$.base64Obj.removeFile($);
				$.base64Obj.fire('cancel', $);
			}
			$.abort = $.remove = $.cancel;

			$.progress = function(){
				return $._prevProgress;
			}

			if(support){
				$.fileReader.onloadend = (function(theFile) {
					return function(e) {
						var res = e.target.result;
						if(!res) return;
						if($._ext === "rtf") res = stripRTF(res);
						else if(!$._type) {
							var data_arr = res.split(':');
							$._type = 'application'; $._ext = 'octet-stream';
							res = [ data_arr[0], ':', $._type, '/', $._ext, data_arr[1] ].join('');
						}

						$._prevProgress = 1;
						$.content = res;
						ev('progress');
						ev('success', JSON.stringify({ name: $.fileName, type: $.type, content: $.content, id: $.uniqueIdentifier }) );    
						$.base64Obj.fire('fileLoaded', $);
					};
				})(file);
				$.fileReader.onerror = (function(theFile) {
					return function(e) { ev('error', e); };
				})(file);
				$.fileReader[$._readAs](file);
				return this;
			}else{
				$.content = (file.content);
				$.base64Obj.fire('fileSuccess',$, JSON.stringify({ name: $.fileName, type: $.type, content: file.content, id: $.uniqueIdentifier }) );    
				$.base64Obj.fire('fileLoaded', file);
		}
	

	}

		// INTERNAL METHODS (both handy and responsible for the heavy load)
		var stripRTF = function(str){
		    var basicRtfPattern = /\{\*?\\[^{}]+}|[{}]|\\[A-Za-z]+\n?(?:-?\d+)?[ ]?/g;
		    var newLineSlashesPattern = /\\\n/g;
		    var bulletsPattern = /(\\\*)?(\\\*\\disc|\\\*\\circle)/g;

		    var stripped = str.replace(basicRtfPattern,"");
		    var removeBullets = stripped.replace(bulletsPattern, "");
		    var removeNewlineSlashes = removeBullets.replace(newLineSlashesPattern, "\n");
		    var removeWhitespace = removeNewlineSlashes.replace(/^\s+|\s+$/g, '');

		    return removeWhitespace;
		}
		var appendFilesFromFileList = function(fileList, event){
			// check for uploading too many files
			var errorCount = 0;
			var o = $.getOpt(['maxFiles', 'minFileSize', 'maxFileSize', 'maxFilesErrorCallback', 'minFileSizeErrorCallback', 'maxFileSizeErrorCallback', 'fileType', 'fileTypeErrorCallback']);

			if (typeof(o.maxFiles)!=='undefined' && o.maxFiles<(fileList.length+$.files.length)) {
				o.maxFilesErrorCallback(fileList, errorCount++);
				return false;
			}
			var files = [];
			$h.each(fileList, function(file){
				file.name = file.fileName = file.fileName || file.name; // consistency across browsers for the error message
				
				if (o.fileType.length > 0 && !$h.contains(o.fileType, file.type.split('/')[1]) && !$h.contains(o.fileType, file.type.split('/')[0])) {
					o.fileTypeErrorCallback(file, errorCount++);
					return false;
				}

				if (typeof(o.minFileSize)!=='undefined' && file.size<o.minFileSize) {
					o.minFileSizeErrorCallback(file, errorCount++);
					return false;
				}
				if (typeof(o.maxFileSize)!=='undefined' && file.size>o.maxFileSize) {
					o.maxFileSizeErrorCallback(file, errorCount++);
					return false;
				}

				// directories have size == 0
				if (file.size > 0 && !$.getFromUniqueIdentifier($h.generateUniqueIdentifier(file))) {
					var f = new base64File($, file);
					$.files.push(f);
					files.push(f);
					$.fire('fileAdded', f, event);
				}
			});
			$.fire('filesAdded', files);
		};

		var onDrop = function(e){
			$h.stopEvent(e);
			appendFilesFromFileList(e.dataTransfer.files, event);
		};

		var onDrag = function(e) {
			$h.stopEvent(e);
		};

		var onChange =  function(e){
			if(!e.target.files) return;
			appendFilesFromFileList(e.target.files);
			e.target.value = '';
		};

		var onClick = function(e){
			e.stopImmediatePropagation();
			if(window.resolveLocalFileSystemURI){
				mobile_upload.call(this, e);
			}
		};

		var onDomNodeClick = function(e){
			e.stopImmediatePropagation();
			if(!$.support && !$.flashSupport) {
				if (AF && AF.alert && AF.alert.close) AF.alert.close();
				$.alert($.getOpt('flashAlert'), "warning", 7000);
				return false;
			}
			var inp = e.target.parentNode.getElementsByTagName("input");
			for(var i in inp){
				if(inp[i].type == "file"){
					inp[i].click(); break;
				}
			}
			return false;
		};

		$.alertSupport = function (domNode) {
			if($.support || $.flashSupport) return;
			if(domNode){
				domNode.addEventListener('click', onDomNodeClick, false);
			} else {
				if (AF && AF.alert && AF.alert.close) AF.alert.close();
				$.alert($.getOpt('flashAlert'), "warning", 7000);
			}
			return false;
		};

		$.assignDrop = function(domNodes){
			if(typeof(domNodes.length)=='undefined') domNodes = [domNodes];

			$h.each(domNodes, function(domNode) {
				//domNode.addEventListener("dragenter", onDrag, false);
				//domNode.addEventListener('dragleave', onDrag, false);
				domNode.addEventListener('dragover', onDrag, false);
				domNode.addEventListener('drop', onDrop, false);
			});
		};

		// PUBLIC METHODS FOR RESUMABLE.JS
		$.assignBrowse = function(domNodes, isDirectory){
			if(typeof(domNodes.length)=='undefined') domNodes = [ domNodes ];
			$.domNodes = domNodes;

			// We will create an <input> and overlay it on the domNode
			// crappy, but since HTML5 doesn't have a cross-browser.browse() method we haven't a choice.
			// FF4+ allows click() for this though: https://developer.mozilla.org/en/using_files_from_web_applications
			$h.each(domNodes, function(domNode) {
				var input;
				if(domNode.tagName==='INPUT' && domNode.type==='file'){
					input = domNode;
				} else {
					input = document.createElement('input');
					
					input.setAttribute('type', 'file');
					input.setAttribute('class', 'resumable-browse-file');
					if(!$.support){
						input.setAttribute('data-url','php/Uploader.php');
            			input.setAttribute('name','import');
					}

					domNode.style.position = 'relative';
					domNode.style.display = 'inline-block';

					input.style.cursor = 'pointer';
					input.style.position = 'absolute';
					input.style.width = domNode.clientWidth + 2 + 'px';
					input.style.height = domNode.clientHeight + 2 + 'px';

					input.style.opacity = input.style.bottom = input.style.right = 0;
					
					var offset = { top: domNode.offsetTop, left: domNode.offsetLeft }
					//if(window.jQuery) { offset = window.jQuery(domNode).position(); }

					input.style.top = offset.top + 'px';
					input.style.left = offset.left + 'px';
					domNode.parentNode.appendChild(input);
					domNode.addEventListener('click', onDomNodeClick, false);

				}
				var multiple = false, maxFiles = $.getOpt('maxFiles');
				if (typeof(maxFiles)==='undefined' || maxFiles!=1){
					input.setAttribute('multiple', 'multiple');
					multiple = true;
				} else {
					input.removeAttribute('multiple');
				}
				if(isDirectory){
					input.setAttribute('webkitdirectory', 'webkitdirectory');
				} else {
					input.removeAttribute('webkitdirectory');
				}

				if($.support) {
					input.addEventListener('click', onClick, false);
					input.addEventListener('change', onChange, false);
				} else {		 
					var _this = $, maxFiles = $.getOpt('maxFiles'), 
					_spinner = window.jQuery(input.parentNode).closest(".fieldContent");
					window.jQuery(input).fileupload({
						limitMultiFileUploadSize :maxFiles,
              			add: function(e, data) {
              				var spn = _this.spinner(_spinner);
             				var xhr = data.submit();
                			xhr.success(function (result, textStatus, jqXHR) {
     							spn.remove();
                 				res =result.contents().find("body #iframe").val();
	                 			var u = JSON.parse(res);
	                 			var f = new base64File($, u,false);
								_this.files.push(f);		
                      		});
                      		xhr.error(function (jqXHR, textStatus, errorThrown){
                      			spn.remove();
                      		});
                		}
              		});
            	}
			});
		};

		$.spinner = function(elem){
			return FS.Utils.spinner(elem, false, {
  				radius: 4,
 				color: "#03C",
				width: 4,
				top: "9px",
				left: "95%"
			});

		};

		$.fallback = function(input, multiple) {			
			if(!window.jQuery) return;

			$.fallbackID = $.fallbackID || FS.Utils.uuid();
			$.fallbackInput = window.jQuery(input).fileReader({
				debugMode: false,
				id: $.fallbackID,
				multiple: multiple,
				filereader: 'swf/filereader.swf',
				expressInstall: 'swf/expressInstall.swf',
				callback: function(){	
					setTimeout(function() {				
						$.fallbackInput.off('.fallback');
						$.fallbackInput.on('click.fallback', onClick).on('change.fallback', onChange);
					});
				}
			})
		};

		$.unAssignDrop = function(domNodes) {
			if (typeof(domNodes.length) == 'undefined') domNodes = [domNodes];

			$h.each(domNodes, function(domNode) {
				//domNode.removeEventListener("dragenter", onDrag);
				//domNode.removeEventListener('dragleave', onDrag);
				domNode.removeEventListener('dragover', onDrag);
				domNode.removeEventListener('drop', onDrop);
			});
		};

		$.unAssignBrowse = function(domNodes) {
			if (typeof(domNodes.length) == 'undefined') domNodes = [domNodes];

			if($.fallbackInput) {
				delete window.FileAPIProxy.inputs[$.fallbackID];
				//window.jQuery(window.FileAPIProxy.swfObject).parent().css('z-index', "-99999");
			}
			
			$h.each(domNodes, function(domNode) {

				if(domNode.tagName==='INPUT' && domNode.type==='file'){
					domNode.removeEventListener('change', onChange);
				} else {
					var inp = domNode.parentNode.getElementsByTagName("input");
					for(var i in inp){
						if(inp[i].type == "file"){
							inp[i].removeEventListener('change', onChange);
							inp[i].parentNode.removeChild(inp[i]);
						}
					}
				}
			});
		};

		$.upload = function(){
			$.fire('uploadStart');
		};

		$.pause = function(){
			// Resume all chunks currently being uploaded
			$h.each($.files, function(file){
				file.abort();
			});
			$.fire('pause');
		};

		$.cancel = function(){
			$h.each($.files, function(file){
				file.cancel();
			});
			$.fire('cancel');
		};

		$.progress = function(){
			var totalDone = 0;
			var totalSize = 0;
			// Resume all chunks currently being uploaded
			$h.each($.files, function(file){
				totalDone += file.progress()*file.size;
				totalSize += file.size;
			});
			return(totalSize>0 ? totalDone/totalSize : 0);
		};

		$.addFile = function(file){
			appendFilesFromFileList([file]);
		};

		$.removeFile = function(file){
			var files = [];
			$h.each($.files, function(f, i){
				if(file.id && ( file.id !== f.uniqueIdentifier ) ) files.push(f);
				else if(!file.id && (f !== file ) ) files.push(f);
				else {
					$.fire('fileRemoved', f);

					if($.fallbackInput) {
						$h.each(window.FileAPIProxy.readers, function(a) {
							if (a.icontains && a.icontains(f.name)){
								delete window.FileAPIProxy.readers[a];
							}
						})
					}
				}
			});
			$.files = files;
		};

		$.getFromUniqueIdentifier = function(uniqueIdentifier){
			var ret = false;
			$h.each($.files, function(f){
				if(f.uniqueIdentifier==uniqueIdentifier) ret = f;
			});
			return(ret);
		};

		$.getSize = function(){
			var totalSize = 0;
			$h.each($.files, function(file){
				totalSize += file.size;
			});
			return(totalSize);
		};
		return(this);
	}
})(window);
